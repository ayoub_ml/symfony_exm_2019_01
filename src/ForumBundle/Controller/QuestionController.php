<?php

namespace ForumBundle\Controller;

use ForumBundle\Entity\Question;
use ForumBundle\Form\QuestionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class QuestionController extends Controller
{
//    public function createAction()
//    {
//        return $this->render('ForumBundle:Question:create.html.twig', array(
//            // ...
//        ));
//    }
//
//    public function readAction()
//    {
//        return $this->render('ForumBundle:Question:read.html.twig', array(
//            // ...
//        ));
//    }
//
//    public function updateAction()
//    {
//        return $this->render('ForumBundle:Question:update.html.twig', array(
//            // ...
//        ));
//    }
//
//    public function deleteAction()
//    {
//        return $this->render('ForumBundle:Question:delete.html.twig', array(
//            // ...
//        ));
//    }



    public function readAction()
    {
        $modeles=$this->getDoctrine()->getRepository(Question::class)->findAll();
        return $this->render('@Forum/Question/read.html.twig', array(
            'modeles'=>$modeles
        ));
    }


    public function createAction(Request $request)
    {
        $question=new Question();
        $form=$this->createForm(QuestionType::class,$question);
        $form=$form->handleRequest($request);
        dump($form);
        if($form->isValid()){
            $em=$this->getDoctrine()->getManager();
            $em->persist($question);
            $em->flush();
            return $this->redirectToRoute('read');
        }
        return $this->render('@Forum/Question/create.html.twig',
            array(
                'form'=>$form->createView()
            ));
    }

    public function updateAction(Request $request , $id)
    {
        $em=$this->getDoctrine()->getManager();
        $modele = $em->getRepository(Question::class)->find($id);
        if ($request->isMethod('POST')) {
            $modele->setTag($request->get('tag'));
            $modele->setTitre($request->get('titre'));
            dump($request->get('tag'));
            $em->flush();
            return $this->redirectToRoute('read');
        }
        return $this->render('@Forum/Question/update.html.twig', array(
            'modele' => $modele
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $question= $em->getRepository(Question::class)->find($id);
        $em->remove($question);
        $em->flush();
        return $this->redirectToRoute("read");
    }

    function queryBuilderAction(){
        $em=$this->getDoctrine()->getManager();
        $modeles= $em->getRepository(Modele::class)->findPaysQB();
        return $this->render('@EspritParc/Modele/AfficheQB.html.twig',array('modeles'=>$modeles));
    }

}
